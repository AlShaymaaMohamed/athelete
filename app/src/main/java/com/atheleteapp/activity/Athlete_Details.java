package com.atheleteapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.atheleteapp.Interface.RequestInterface;
import com.atheleteapp.JSONResponse;
import com.atheleteapp.R;
import com.atheleteapp.model.AthelteModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Athlete_Details extends AppCompatActivity {

    int position;
    int keyposition;
    ArrayList<AthelteModel> data;
    @Bind(R.id.title_details)
    TextView title;
    @Bind(R.id.breif_details)
    TextView breif;
    @Bind(R.id.img_details)
    ImageView img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_athlete__details);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
         keyposition=getIntent().getIntExtra("position",position);
        loadJSON();
    }
    private void loadJSON(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://gist.githubusercontent.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<JSONResponse> call = request.getJSON();
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                data = new ArrayList<>(Arrays.asList(jsonResponse.getAndroid()));
               AthelteModel selectedItem=data.get(keyposition);
                title.setText(selectedItem.getName());
                String path=selectedItem.getImage();
                if(!path.isEmpty()) {
                    Picasso.with(Athlete_Details.this).load(selectedItem.getImage()).into(img);
                }
                else{
                    Picasso.with(Athlete_Details.this).load(R.drawable.image).into(img);
                }
                breif.setText(selectedItem.getBrief());
             }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });
    }
}
