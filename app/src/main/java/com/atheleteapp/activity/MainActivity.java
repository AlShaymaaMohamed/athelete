package com.atheleteapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.atheleteapp.Interface.OnHolderClicked;
import com.atheleteapp.Interface.RequestInterface;
import com.atheleteapp.JSONResponse;
import com.atheleteapp.R;
import com.atheleteapp.adapter.DataAdapter;
import com.atheleteapp.model.AthelteModel;
import com.atheleteapp.presenter.AtheletPresenter;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity  {
   @Bind(R.id.card_recycler_view)
   RecyclerView recyclerView;
    private ArrayList<AthelteModel> data;
    private DataAdapter adapter;
    private AtheletPresenter atheletPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        atheletPresenter = new AtheletPresenter();
       // atheletPresenter.attachedView(this);
        initViews();
    }
    private void initViews(){

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        loadJSON();
    }
    private void loadJSON(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://gist.githubusercontent.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<JSONResponse> call = request.getJSON();
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                Log.e("aplication",jsonResponse.toString());
                data = new ArrayList<>(Arrays.asList(jsonResponse.getAndroid()));

              adapter=new DataAdapter(data, getApplicationContext(), new OnHolderClicked() {
                  @Override
                  public void onClick(int position) {
                      Intent intent= new Intent(MainActivity.this,Athlete_Details.class);
                      intent.putExtra("position",position);
                        startActivity(intent);
                  }
              });

                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error",t.getMessage());
            }
        });
    }
    /*
    @Override
    public void setItems(final ArrayList<AthelteModel> Athdata) {

        adapter=new DataAdapter(Athdata, getApplicationContext(), new OnHolderClicked() {
            @Override
            public void onClick(int position) {
                Intent intent= new Intent(MainActivity.this,Athlete_Details.class);
                intent.putExtra("position",position);
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(adapter);

    }
    @Override public void onResume() {
        super.onResume();
        atheletPresenter.onResume();
    }
    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage(String message) {

    }*/

}

