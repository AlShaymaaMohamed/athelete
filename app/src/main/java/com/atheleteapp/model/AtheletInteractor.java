package com.atheleteapp.model;

import android.os.Handler;
import android.util.Log;

import com.atheleteapp.Interface.RequestInterface;
import com.atheleteapp.JSONResponse;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Green on 28/11/2017.
 */

public class AtheletInteractor implements Interactor {
ArrayList <AthelteModel> data;

    @Override
    public void loadItems(final LoaderListener loaderListener) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loaderListener.onFinished(createCollectionPictures());
            }
        }, 2000);

    }
    private ArrayList<AthelteModel> createCollectionPictures() {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://gist.githubusercontent.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<JSONResponse> call = request.getJSON();
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                Log.e("aplication",jsonResponse.toString());
                data = new ArrayList<>(Arrays.asList(jsonResponse.getAndroid()));
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {

            }
        });

        return data;

    }

    }




