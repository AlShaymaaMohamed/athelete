package com.atheleteapp.model;

/**
 * Created by Green on 28/11/2017.
 */

public class AthelteModel {
    private String name;
    private String image;
    private String brief;

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getBrief() {
        return brief;
    }
}
