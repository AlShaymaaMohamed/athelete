package com.atheleteapp.model;


public interface Interactor {

    void loadItems(LoaderListener loaderListener);
}
