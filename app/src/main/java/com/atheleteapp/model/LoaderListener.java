package com.atheleteapp.model;

import java.util.ArrayList;


public interface LoaderListener {

    void onFinished(ArrayList<AthelteModel> pictureList);
}
