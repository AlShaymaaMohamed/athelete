package com.atheleteapp.view;

import com.atheleteapp.model.AthelteModel;

import java.util.ArrayList;

/**
 * Created by Green on 28/11/2017.
 */

public interface AtheletModelMVPview {
    void setItems(ArrayList<AthelteModel> pictureList);

    void showProgress();

    void hideProgress();

    void showMessage(String message);
}
